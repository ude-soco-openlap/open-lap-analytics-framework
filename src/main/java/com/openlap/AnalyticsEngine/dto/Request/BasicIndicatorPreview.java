package com.openlap.AnalyticsEngine.dto.Request;

import com.openlap.AnalyticsEngine.dto.Response.IndicatorResponse;
import lombok.Data;

import java.util.List;

/**
 Created By Ao on 2023/2/5
 */
@Data
public class BasicIndicatorPreview {
    String name;
    String id;
    String indicatorRequestCode;
    String analyticsMethodId;
    String analyticsMethodName;
    private List<IndicatorResponse> indicators;

}
